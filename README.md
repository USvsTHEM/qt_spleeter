[![Licence: GPL 3.0](https://img.shields.io/badge/licence-GPL--3.0-lightgrey)](LICENSE)

[![Licence: TODO](https://img.shields.io/badge/package-Appimage-green)]()


# Spleete QT GUI
This codebase is a QT gui for [spleeter](https://github.com/deezer/spleeter)

![sample image](images/image.png)

Download appimage for linux [here](https://gitlab.com/nicolalandro/qt_spleeter/-/blob/master/bin/Qt_Spleeter-x86_64.AppImage) (!!!! OLD VERSION, TODO, Run the dev). For the first run is preferable to run by command line because it install the requirements.

## Dev

* Run for dev

```
sudo apt install ffmpeg qt5-default
# python 3.7 (or maybe python>=3)
pip install -r ./src/requirements.txt
cd src
python qt_spleeter.py
# or you can also specify your Spleeter version and your spleeter command
SPLEETER_COMMAND="python3.7 -m spleeter" SPLEETER_VERSION="2.1.0" python qt_spleeter.py

```

* Run with appimage python
```
cd src
./python3.7.8-cp37-cp37m-manylinux1_x86_64.AppImage qt_spleeter.py
```

* Create Appimage 
```
./scripts/create_appimage.sh
```

